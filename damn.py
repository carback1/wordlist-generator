import nltk
import re

# nltk.download('wordnet')
# nltk.download('wordnet2021')
# nltk.download('omw-1.4')
# nltk.download('wordnet_ic')
# nltk.download('averaged_perceptron_tagger')

from nltk.corpus import wordnet2021 as wordnet
#from nltk.corpus import wordnet
from nltk.corpus import wordnet_ic

invalid_characters = re.compile('[^a-zA-Z\-_ ]')

base_dir = './'


# Import bad words set
bad_words = []
with open(f'{base_dir}/bad-words.txt', 'r') as bad_words_file:
    for w in bad_words_file:
        # Note: spaces are intentional here to prevent overmatching
        word = w.strip('\n').lower()
        if len(word) >= 3 and not invalid_characters.search(word):
            bad_words.append(word)
#deduplicate
bad_words = list(set(bad_words))
#print(bad_words)

# Generate banned word types (company names and trademarks) 
# NOTE: To find these, iterate over all "synsets(word, pos='n')" and print the .definition()
# FIXME: Disabled because it didn't really work
banned_word_types = [wordnet.synset(x) 
                     for x in [
#                              'company.n.01',
#                              'trademark.n.02',
                              ]]

banned_lexnames = set(['noun.person'])

dict_words = set([x.strip().lower() for x in open('/usr/share/dict/words', 'r')])
#print(f'dict_words: {len(dict_words)}')

company_names = []
with open(f'{base_dir}/company-names.txt', 'r') as cnames:
    with open(f'{base_dir}/company-names-dictwords.txt', 'w') as dwords:
        for w in cnames:
            w = w.strip().lower()
            w = w.split()[0]
            if w in dict_words:
                print(f"dictionary word: {w}", file=dwords)
                continue
            if len(w) <= 3:
                continue

            company_names.append(w)
company_names = list(set(company_names))
#print(f'company_names: {len(company_names)}')

def replace_spacers(word):
    '''replace_spacers replaces -, _, and spaces with firstSecond format'''
    for c in ['-', '_', ' ']:
        parts = word.split(c)
        if len(parts) == 1:
            continue
        
        # now camelcase it
        word = parts[0]
        for i in range(1, len(parts)):
            if len(parts[i]) == 0:
                continue
            word = word + parts[i][0].upper() + parts[i][1:]

    return word
    

def valid_word(word):
    '''valid_word returns a tuple of (true, '') if the word is valid
    or (false, reasonstring) if not. We assume the word is all lower case'''
    if len(word) <= 3:
        return (False, "3 or less characters")
    if invalid_characters.search(word):
        return (False, 'invalid characters')
    
    w = word.lower()
    for bad in bad_words:
        if w.startswith(bad) or bad[0:-1] == w:
            return (False, f'in bad words list, matched "{bad}"')            

    # Skip the check if it's in our common system dictionary
    if word in dict_words:
        return (True, '')

    # Note we only care about nouns right now...
    brown_ic = wordnet_ic.ic('ic-brown.dat')
    synsets = wordnet.synsets(word, pos='n')
    if len(synsets) == 0:
        return (False, f'no synsets for {word}')
    for ss in synsets:
        # Only apply this filter to multi-word elements
        if ss.lexname() in banned_lexnames and '_' in word:
            return(False, f'banned lexname {ss.lexname()}')
        for banned in banned_word_types:
            similarity_metric = ss.jcn_similarity(banned, brown_ic)
            if similarity_metric < 0.01:
                return(False, f'banned word type: {banned}, {similarity_metric}')
        break # Only check the first word
    
    return (True, '')


from multiprocessing import Pool
import sys
import tqdm

def proc_word(l):
    l = l.strip()

    # comment this out when using lemma_names
    for ltr in l:
        if ltr.isupper():
            l = l.replace(ltr, '_' + ltr.lower())
            #print(l)

    l = l.lower()
    is_valid, reason = valid_word(l)
    if is_valid:
        print(replace_spacers(l), flush=True)
    else:
        print(l + ',' + reason, file=sys.stderr)
    return (l, is_valid, reason)


def f(x):
    return x**2

if __name__ == '__main__':
    pool = Pool(4)
    for res in pool.map(f,range(20)):
        print(res)

    cnt = 0
    bad_cnt = 0
    with open(f'{base_dir}/nouns.txt', 'w') as nouns:
        with open(f'{base_dir}/bad-nouns.txt', 'w') as bad_nouns:
            #for l in wordnet.all_lemma_names(pos='n', lang='eng'):
            #for l in open(f'{base_dir}/orig-nouns.txt', 'r').readlines():
            words = [w for w in open(f'{base_dir}/orig-nouns.txt', 'r').readlines()]
            with Pool(16) as p:
                for l, is_valid, reason in tqdm.tqdm(p.imap_unordered(proc_word, words), total=len(words)):
                    if is_valid:
                        print(replace_spacers(l), file=nouns)
                        cnt += 1
                    else:
                        print(l + ',' + reason, file=bad_nouns)
                        bad_cnt += 1
                    print(cnt + bad_cnt, len(words))
    print("Nouns: ", cnt, bad_cnt)

#cnt = 0
#bad_cnt = 0
#with open('verbs.txt', 'w') as verbs:
#    with open('bad-verbs.txt', 'w') as bad_verbs:
#        for l in wordnet.all_lemma_names(pos='v', lang='eng'):
#            l = l.lower()
#            is_valid, reason = valid_word(l)
#            if is_valid:
#                print(replace_spacers(l), file=verbs)
#                cnt += 1
#            else:
#                print(l + ',' + reason, file=bad_verbs)
#                bad_cnt += 1
#print("Verbs: ", cnt, bad_cnt)
    
#cnt = 0
#bad_cnt = 0
#with open(f'{base_dir}/adjectives.txt', 'w') as adjectives:
#    with open(f'{base_dir}/bad-adjectives.txt', 'w') as bad_adjectives:
#        for l in wordnet.all_lemma_names(pos='a', lang='eng'):
#            l = l.lower()
#            is_valid, reason = valid_word(l)
#            if is_valid:
#                print(replace_spacers(l), file=adjectives)
#                cnt += 1
#            else:
#                print(l + ',' + reason, file=bad_adjectives)
#                bad_cnt += 1   
#print("Adjectives: ", cnt, bad_cnt)

# Note: we might want to add articles (also often called determiners) e.g., 'the', 'a', etc
